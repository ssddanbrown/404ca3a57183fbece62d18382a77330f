This is a tested setup of using [LSIO Swag](https://docs.linuxserver.io/general/swag) with [LSIO's BookStack container](https://docs.linuxserver.io/images/docker-bookstack)

### Environment

This was my environment and are therefore details you'll need to change:

- I was using `lsiotesting.danb.me` as the main domain for swag.
- I was hosting bookstack on a subdomain of that (`bookstack.lsiotesting.danb.me`).
- I was storing my docker mounted files at `/root/docker` (Verly likely bad practice but was easy place for testing).

I was on a fresh Ubuntu 20.04.1 instance on digitalocean, with `docker.io` and `docker-compose` installed via apt.

### Actions Taken

1. I pointed both my main domain (`lsiotesting.danb.me`) and my bookstack domain (`bookstack.lsiotesting.danb.me`) at my server IP address.
2. I added the above docker-compose.yml onto the server.
3. I ran `docker-compose up` with the above compose.
4. After a few mins, once the output of (3) had calmed down (After it had migrated the bookstack database and fetched the HTTPS certs) I stopped docker-compose to bring down the containers.
5. Within the `$SWAG_MOUNT_FOLDER_PATH/nginx/proxy-confs` folder, I activated the bookstack config: `mv bookstack.subdomain.conf.sample bookstack.subdomain.conf`
6. I restarted the containers.
7. BookStack instance all live on https at `https://bookstack.lsiotesting.danb.me`.